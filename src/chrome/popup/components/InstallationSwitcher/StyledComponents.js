import styled from 'styled-components'

import {ButtonLink, Title} from '../Styled'

export const StyledTitle = styled.h2`
  ${Title}
`

export const StyledButton = styled.button`
  ${ButtonLink}
  margin-right: 10px;

  &:last-child {
    margin-right: 0px;
  }
`
