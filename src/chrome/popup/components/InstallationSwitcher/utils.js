export const getBaseUrl = subdomain => {
  if (subdomain.endsWith('test')) {
    return subdomain.slice(0, -4)
  }
  if (subdomain.endsWith('testnew') || subdomain.endsWith('testold')) {
    return subdomain.slice(0, -7)
  }
  return subdomain
}

export const getPossibleUrls = baseUrl => [
  {
    name: 'Prod',
    checkUrl: 'https://' + baseUrl + '.tocco.ch/status-tocco',
    redirectUrl: 'https://' + baseUrl + '.tocco.ch/status-tocco'
  },
  {
    name: 'Test',
    checkUrl: 'https://' + baseUrl + 'test.tocco.ch/status-tocco',
    redirectUrl: 'https://' + baseUrl + 'test.tocco.ch/support-tocco'
  },
  {
    name: 'Test New',
    checkUrl: 'https://' + baseUrl + 'testnew.tocco.ch/status-tocco',
    redirectUrl: 'https://' + baseUrl + 'testnew.tocco.ch/support-tocco'
  },
  {
    name: 'Test Old',
    checkUrl: 'https://' + baseUrl + 'testold.tocco.ch/status-tocco',
    redirectUrl: 'https://' + baseUrl + 'testold.tocco.ch/support-tocco'
  }
]
