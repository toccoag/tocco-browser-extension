import {useEffect, useState} from 'react'

import {StyledButton, StyledTitle} from './StyledComponents'
import {getBaseUrl, getPossibleUrls} from './utils'
import Loader from '../Loader'
import {navigateTo} from '../../../chrome-api'
import {useSettingsContext} from '../Settings/SettingsContext'

const InstallationSwitcher = () => {
  const [loading, setLoading] = useState(false)
  const {url} = useSettingsContext()
  const [installationUrls, setInstallationUrls] = useState()

  const getInstallationUrls = async () => {
    setLoading(true)
    const hostParts = url.host.split('.')
    if (hostParts.length !== 3 || hostParts[1] !== 'tocco' || hostParts[2] !== 'ch') {
      setInstallationUrls([])
      setLoading(false)
      return
    }

    const baseUrl = getBaseUrl(hostParts[0])
    const possibleUrls = getPossibleUrls(baseUrl)

    const checkedUrls = await Promise.all(
      possibleUrls.map(possibleUrl =>
        fetch(possibleUrl.checkUrl)
          .then(({status}) => (status === 200 ? possibleUrl : null))
          .catch(() => null /* url not available */)
      )
    )
    const availableUrls = checkedUrls.filter(t => t)

    setInstallationUrls(availableUrls)
    setLoading(false)
  }

  useEffect(() => {
    getInstallationUrls()
  }, [])

  if (!loading && (!installationUrls || installationUrls.length === 0)) {
    return null
  }

  const content = installationUrls ? (
    <>
      {installationUrls.map(({redirectUrl, name}) => (
        <StyledButton type="button" onClick={() => navigateTo(redirectUrl)} key={name}>
          {name}
        </StyledButton>
      ))}
    </>
  ) : null

  return (
    <>
      <StyledTitle>Installationen</StyledTitle>
      {loading ? <Loader /> : content}
    </>
  )
}

export default InstallationSwitcher
