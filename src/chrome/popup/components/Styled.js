import {css} from 'styled-components'

export const ToccoColor = '#B22A31'

export const Title = css`
  font-size: 14px;
`

export const SubTitle = css`
  font-size: inherit;
  font-weight: normal;
`

export const Button = css`
  border: 1px solid ${ToccoColor};
  background-color: ${ToccoColor};
  color: white;
  border-radius: 40px;
  line-height: 1;
  outline: none;
  cursor: pointer;
  padding: 5px 7px;
  text-align: center;
`

export const ButtonLight = css`
  ${Button}
  border: 1px solid ${ToccoColor};
  background-color: white;
  color: ${ToccoColor};
`

export const ButtonLink = css`
  border: 0px;
  padding: 0px;
  outline: none;
  cursor: pointer;
  text-decoration: underline;
  color: ${ToccoColor};
  background-color: transparent;
  text-align: left;
  display: inline-block;
`
