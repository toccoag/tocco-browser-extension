import {StyledButton} from './StyledComponents'
import {useSettingsContext} from '../Settings/SettingsContext'

const copyToClipboard = text => {
  if (!navigator.clipboard) {
    return Promise.reject(new Error('navigator.clipboard API not available'))
  }

  return navigator.clipboard.writeText(text)
}

const MetaInformation = () => {
  const {url, clientSettings, tab} = useSettingsContext()

  const handleCopyMetaInformation = async () => {
    const metaInformation = `URL: ${url.href}
User agent: ${window.navigator.userAgent}
Window size: ${tab.width}px x ${tab.height}px
Nice version: ${clientSettings.niceVersion}
Nice revision: ${clientSettings.niceRevision}`

    try {
      await copyToClipboard(metaInformation)
    } catch (e) {
      console.error(e)
    }
  }

  return (
    <StyledButton type="button" onClick={() => handleCopyMetaInformation()}>
      Seiteninformation in Zwischenablage kopieren
    </StyledButton>
  )
}

export default MetaInformation
