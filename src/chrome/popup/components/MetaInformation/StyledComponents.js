import styled from 'styled-components'

import {ButtonLink} from '../Styled'

export const StyledButton = styled.button`
  ${ButtonLink}
  margin-top: 10px;
  margin-bottom: 10px;
`
