import styled from 'styled-components'

export const StyledWrapper = styled.div`
  display: grid;
  grid-template-areas: 'label value';
  row-gap: 10px;
`

export const StyledLabel = styled.div`
  grid-area: 'label';
`

export const StyledValue = styled.div`
  grid-area: 'value';
  font-family: 'Courier New', Courier, monospace;
`
