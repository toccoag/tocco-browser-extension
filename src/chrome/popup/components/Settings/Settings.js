import {StyledLabel, StyledValue, StyledWrapper} from './StyledComponents'
import {useSettingsContext} from './SettingsContext'

const Settings = () => {
  const {clientSettings} = useSettingsContext()

  const buildTime = new Date(clientSettings.buildTimestamp).toLocaleString()

  return (
    <StyledWrapper>
      <StyledLabel>Nice Version:</StyledLabel>
      <StyledValue>{clientSettings.niceVersion}</StyledValue>

      <StyledLabel>Environment:</StyledLabel>
      <StyledValue>{clientSettings.runEnv}</StyledValue>

      <StyledLabel>Build Timestamp:</StyledLabel>
      <StyledValue>{buildTime}</StyledValue>
    </StyledWrapper>
  )
}

export default Settings
