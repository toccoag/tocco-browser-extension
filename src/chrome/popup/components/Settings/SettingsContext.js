import {createContext, useContext, useEffect, useState} from 'react'

import Loader from '../Loader'
import {fetchClientSettings} from '../../../../tocco-api'
import {getCurrentTab} from '../../../chrome-api'

const defaultValue = null
const SettingsContext = createContext(defaultValue)

const SettingsContextProvider = ({children}) => {
  const [loading, setLoading] = useState(false)
  const [settings, setSettings] = useState(null)

  const getClientSettings = async () => {
    setLoading(true)
    try {
      const tab = await getCurrentTab()
      const url = tab ? new URL(tab.url) : null
      if (url) {
        const clientSettings = await fetchClientSettings(url.origin)
        setSettings({clientSettings, url, tab})
      }
    } catch (e) {
      console.error(e)
      setSettings(null)
    }
    setLoading(false)
  }

  useEffect(() => {
    getClientSettings()
  }, [])

  const value = settings
  if (loading) {
    return <Loader />
  }

  return <SettingsContext.Provider value={value}>{children}</SettingsContext.Provider>
}

export const useSettingsContext = () => useContext(SettingsContext)
export default SettingsContextProvider
