import {createContext, useContext, useEffect, useState} from 'react'

import {getWidgetConfigKeys, getWidgetDomain} from '../../../../../widget-api'
import Loader from '../../Loader'
import {fetchWidgetConfig} from '../../../../../tocco-api'
import {getDOM} from '../../../../chrome-api'

const defaultValue = null
const WidgetSettingsContext = createContext(defaultValue)

const WidgetSettingsContextProvider = ({children}) => {
  const [loading, setLoading] = useState(true)
  const [settings, setSettings] = useState(null)

  useEffect(() => {
    setLoading(true)
    const extractWidgetConfigFromDOM = async domContent => {
      if (domContent) {
        try {
          const parser = new DOMParser()
          const doc = parser.parseFromString(domContent, 'text/html')
          const baseUrl = getWidgetDomain(doc)

          if (baseUrl) {
            const widgetConfigKeys = getWidgetConfigKeys(doc)

            const widgetConfigs = await Promise.all(
              widgetConfigKeys.map(key => fetchWidgetConfig(baseUrl, key).then(config => ({uniqueId: key, ...config})))
            )
            console.log(widgetConfigs)
            setSettings({
              baseUrl,
              widgetConfigs
            })
          } else {
            setSettings(null)
          }
        } catch (error) {
          console.log('error', error)
          setSettings(null)
        }
      }
      setLoading(false)
    }

    try {
      getDOM(extractWidgetConfigFromDOM)
    } catch (_error) {
      setLoading(false)
    }
  }, [])

  const value = settings
  if (loading) {
    return <Loader />
  }

  return <WidgetSettingsContext.Provider value={value}>{children}</WidgetSettingsContext.Provider>
}

export const useWidgetSettingsContext = () => useContext(WidgetSettingsContext)
export default WidgetSettingsContextProvider
