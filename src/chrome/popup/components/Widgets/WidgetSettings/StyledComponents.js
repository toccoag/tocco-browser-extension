import styled from 'styled-components'

export const StyledWrapper = styled.div`
  display: grid;
  grid-template-areas: 'label value';
  row-gap: 10px;
`

export const StyledLabel = styled.div`
  grid-area: 'label';
`

export const StyledValue = styled.div`
  grid-area: 'value';
  font-family: 'Courier New', Courier, monospace;
`

export const StyledListValue = styled.ul`
  grid-area: 'value';
  font-family: 'Courier New', Courier, monospace;
  margin: 0;
  padding: 0;
  list-style-type: none;

  &&& li {
    margin-bottom: 10px;
  }
`
