import {StyledLabel, StyledListValue, StyledValue, StyledWrapper} from './StyledComponents'
import {useWidgetSettingsContext} from './WidgetSettingsContext'

const WidgetSettings = () => {
  const {baseUrl, widgetConfigs} = useWidgetSettingsContext()

  return (
    <>
      <StyledWrapper>
        <StyledLabel>Tocco Instanz:</StyledLabel>
        <StyledValue>{baseUrl}</StyledValue>

        <StyledLabel>Widgets:</StyledLabel>
        <StyledListValue>
          {widgetConfigs.map(c => (
            <li key={c.uniqueId}>{c.uniqueId}</li>
          ))}
        </StyledListValue>
      </StyledWrapper>
    </>
  )
}

export default WidgetSettings
