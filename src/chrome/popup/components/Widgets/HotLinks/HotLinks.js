import {useEffect, useState} from 'react'

import {StyledButton, StyledButtonWrapper, StyledSubTitle, StyledTitle} from './StyledComponents'
import {fetchProperties} from '../../../../../tocco-api'
import {openTab} from '../../../../chrome-api'
import {useWidgetSettingsContext} from '../WidgetSettings/WidgetSettingsContext'

const newAdmin = 'nice2.web.core.newAdminUrl'

const HotLinks = () => {
  const [properties, setProperties] = useState()
  const {baseUrl, widgetConfigs} = useWidgetSettingsContext()

  const loadPropertyValues = async () => {
    try {
      const propertyKeys = [newAdmin]
      const values = await fetchProperties(baseUrl, propertyKeys)
      setProperties(values)
    } catch (e) {
      console.error(e)
    }
  }

  useEffect(() => {
    loadPropertyValues()
  }, [])

  const hasWidgetConfigs = widgetConfigs && widgetConfigs.length > 0
  const content = hasWidgetConfigs ? (
    <>
      <StyledSubTitle>Widget-Konfigurationen</StyledSubTitle>
      <StyledButtonWrapper>
        {widgetConfigs.map(({uniqueId, key}) => (
          <StyledButton
            type="button"
            onClick={async () => openTab(`${baseUrl}/tocco/e/Widget_config/${key}/detail`)}
            key={key}
          >
            {uniqueId}
          </StyledButton>
        ))}
      </StyledButtonWrapper>
    </>
  ) : null

  const adminUrl = properties ? `${baseUrl}/${properties[newAdmin]}` : baseUrl

  return (
    <>
      <StyledTitle>Links</StyledTitle>
      <StyledButton type="button" onClick={async () => openTab(adminUrl)} $single>
        Tocco Instanz
      </StyledButton>

      {content}
    </>
  )
}

export default HotLinks
