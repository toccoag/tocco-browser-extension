import HotLinks from './HotLinks'
import WidgetSettings from './WidgetSettings'

const Widgets = () => (
  <>
    <WidgetSettings />
    <HotLinks />
  </>
)

export default Widgets
