import styled from 'styled-components'

import SettingsContextProvider, {useSettingsContext} from './Settings/SettingsContext'
import WidgetSettingsContextProvider, {useWidgetSettingsContext} from './Widgets/WidgetSettings/WidgetSettingsContext'
import AdminSwitcher from './AdminSwitcher'
import Configurations from './Configurations'
import HotLinks from './HotLinks'
import InstallationSwitcher from './InstallationSwitcher'
import MetaInformation from './MetaInformation'
import Settings from './Settings'
import {ToccoColor} from './Styled'
import Widgets from './Widgets'

const StyledTitle = styled.h1`
  color: ${ToccoColor};
  font-size: 20px;
  margin: 0px 0px 20px;
`

const StyledApp = styled.div`
  width: 320px;
  padding: 5px;
  font-size: 14px;
`

const Admin = () => (
  <>
    <Settings />
    <MetaInformation />
    <AdminSwitcher />
    <HotLinks />
    <InstallationSwitcher />
    <Configurations />
  </>
)

const Content = () => {
  const widgetSettingsContext = useWidgetSettingsContext()
  const settingsContext = useSettingsContext()

  if (!widgetSettingsContext && !settingsContext) {
    return <div>Die Tocco Extension ist nur auf Tocco Instanzen verfügbar.</div>
  }

  return (
    <>
      {widgetSettingsContext && <Widgets />}
      {settingsContext && <Admin />}
    </>
  )
}

const App = () => {
  return (
    <StyledApp>
      <StyledTitle>Tocco Extension</StyledTitle>
      <WidgetSettingsContextProvider>
        <SettingsContextProvider>
          <Content />
        </SettingsContextProvider>
      </WidgetSettingsContextProvider>
    </StyledApp>
  )
}

export default App
