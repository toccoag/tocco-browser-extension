import styled from 'styled-components'

import {Button, ButtonLight, Title} from '../Styled'

export const StyledTitle = styled.h2`
  ${Title}
`

export const StyledButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: -5px;
`

export const StyledConfigurationButton = styled.button`
  ${ButtonLight}
  margin: 5px;
`

export const StyledButton = styled.button`
  ${Button}
  margin: 5px;
`
