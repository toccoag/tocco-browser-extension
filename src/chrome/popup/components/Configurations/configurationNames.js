export default {
  reports: 'Reports',
  acl: 'ACL',
  menu: 'Menü',
  textresources: 'Textresourcen',
  cms: 'CMS-Konfiguration',
  form: 'Forms',
  data: 'Datenobjekte/Relationen'
}
