import {useEffect, useState} from 'react'

import {StyledButton, StyledButtonWrapper, StyledConfigurationButton, StyledTitle} from './StyledComponents'
import {fetchConfigurations, reloadConfigurations} from '../../../../tocco-api'
import Loader from '../Loader'
import configurationNames from './configurationNames'
import {useSettingsContext} from '../Settings/SettingsContext'

const Configurations = () => {
  const [loading, setLoading] = useState(false)
  const [configurations, setConfigurations] = useState()
  const {url} = useSettingsContext()

  const getConfigurations = async () => {
    setLoading(true)
    try {
      const settings = await fetchConfigurations(url.origin)
      setConfigurations(settings)
    } catch (e) {
      console.error(e)
      setConfigurations(null)
    }
    setLoading(false)
  }

  const reloadConfiguration = async id => {
    setLoading(true)
    try {
      await reloadConfigurations(url.origin, [id])
    } catch (e) {
      console.error(e)
    }
    setLoading(false)
  }

  useEffect(() => {
    getConfigurations()
  }, [])

  if (!loading && !configurations) {
    return null
  }

  const content = configurations ? (
    <>
      <StyledButtonWrapper>
        {configurations.map(({id}) => (
          <StyledConfigurationButton key={id} type="button" onClick={() => reloadConfiguration(id)}>
            {configurationNames[id] || id}
          </StyledConfigurationButton>
        ))}
        <StyledButton type="button" onClick={() => reloadConfiguration(configurations.map(({id}) => id))}>
          Alle neu laden
        </StyledButton>
      </StyledButtonWrapper>
    </>
  ) : null

  return (
    <>
      <StyledTitle>Konfigurationen neu laden</StyledTitle>
      {loading ? <Loader /> : content}
    </>
  )
}

export default Configurations
