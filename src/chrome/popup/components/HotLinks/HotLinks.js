import {useEffect, useState} from 'react'

import {StyledButton, StyledButtonWrapper, StyledSubTitle, StyledTitle} from './StyledComponents'
import {getCurrentUrl, openTab} from '../../../chrome-api'
import Loader from '../Loader'
import {fetchWidgetDomains} from '../../../../tocco-api'
import {isNiceVersionGreaterOrEqThan} from '../../../../util'
import {useSettingsContext} from '../Settings/SettingsContext'

const HotLinks = () => {
  const [loading, setLoading] = useState(false)
  const [widgetDomains, setWidgetDomains] = useState()
  const {url, clientSettings} = useSettingsContext()

  const getWidgetDomains = async () => {
    setLoading(true)
    try {
      const domains = await fetchWidgetDomains(url.origin)
      setWidgetDomains(domains)
    } catch (e) {
      console.error(e)
      setWidgetDomains(null)
    }
    setLoading(false)
  }

  useEffect(() => {
    getWidgetDomains()
  }, [])

  const getWidgetShowcaseUrl = async () => {
    const url = await getCurrentUrl()
    return `${url.origin}/widget-showcase`
  }

  const getStatusUrl = async () => {
    const url = await getCurrentUrl()
    return `${url.origin}/status-tocco`
  }

  const getWidgetDomainUrl = domain => {
    return `https://${domain}`
  }

  const hasWidgetShowcase = isNiceVersionGreaterOrEqThan(clientSettings.niceVersion, '3.7')
  const hasWidgetDomains = widgetDomains && widgetDomains.length > 0

  const content = hasWidgetDomains ? (
    <>
      <StyledSubTitle>Widget-Domains</StyledSubTitle>
      <StyledButtonWrapper>
        {widgetDomains.map(
          ({
            key,
            paths: {
              domain: {value: domain}
            }
          }) => (
            <StyledButton key={key} type="button" onClick={async () => openTab(getWidgetDomainUrl(domain))}>
              {domain}
            </StyledButton>
          )
        )}
      </StyledButtonWrapper>
    </>
  ) : null

  return (
    <>
      <StyledTitle>Links</StyledTitle>
      <StyledButton type="button" onClick={async () => openTab(await getStatusUrl())} $single>
        Status Tocco
      </StyledButton>
      {hasWidgetShowcase && (
        <StyledButton type="button" onClick={async () => openTab(await getWidgetShowcaseUrl())} $single>
          Widget-Showcase
        </StyledButton>
      )}
      {loading ? <Loader /> : content}
    </>
  )
}

export default HotLinks
