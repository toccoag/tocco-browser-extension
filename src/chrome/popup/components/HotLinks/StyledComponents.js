import styled, {css} from 'styled-components'

import {ButtonLink, SubTitle, Title} from '../Styled'

export const StyledTitle = styled.h2`
  ${Title}
`

export const StyledSubTitle = styled.h3`
  ${SubTitle}
`

export const StyledButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
`

export const StyledButton = styled.button`
  ${ButtonLink}
  ${({$single}) =>
    $single &&
    css`
      display: block;
      margin-top: 5px;
      margin-bottom: 5px;
    `}
`
