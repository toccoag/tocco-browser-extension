import {useEffect, useState} from 'react'

import {getCurrentUrl, openTab} from '../../../chrome-api'
import {StyledButton} from './StyledComponents'
import {fetchProperties} from '../../../../tocco-api'
import {useSettingsContext} from '../Settings/SettingsContext'

const oldAdmin = 'nice2.web.core.oldAdminUrl'
const newAdmin = 'nice2.web.core.newAdminUrl'

const AdminSwitcher = () => {
  const [properties, setProperties] = useState()
  const {url} = useSettingsContext()

  const loadPropertyValues = async () => {
    try {
      const propertyKeys = [oldAdmin, newAdmin]
      const values = await fetchProperties(url.origin, propertyKeys)
      setProperties(values)
    } catch (e) {
      console.error(e)
    }
  }

  useEffect(() => {
    loadPropertyValues()
  }, [])

  const getOtherAdminUrl = async () => {
    const url = await getCurrentUrl()
    const path =
      url && url.pathname.startsWith(`/${properties[newAdmin]}/`) ? properties[oldAdmin] : properties[newAdmin]

    return `${url.origin}/${path}`
  }

  if (!properties) {
    return null
  }

  return (
    <StyledButton type="button" onClick={async () => openTab(await getOtherAdminUrl())}>
      Admin wechseln
    </StyledButton>
  )
}

export default AdminSwitcher
