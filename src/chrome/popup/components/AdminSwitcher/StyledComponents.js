import styled from 'styled-components'

import {Button} from '../Styled'

export const StyledButton = styled.button`
  ${Button}
  margin-top: 10px;
  margin-bottom: 10px;
  width: 100%;
`
