const DOM_MESSAGE_ID = 'getDOM'

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
  if (msg.text === DOM_MESSAGE_ID) {
    sendResponse(document.documentElement.outerHTML)
  }
})
