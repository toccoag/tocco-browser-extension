import React, {useRef, useState} from 'react'
import {
  arrow,
  flip,
  FloatingArrow,
  FloatingPortal,
  offset,
  shift,
  useClick,
  useDismiss,
  useFloating,
  useInteractions
} from '@floating-ui/react'

import {StyledTooltip} from './StyledComponents'

const useTooltip = () => {
  const [isOpen, setIsOpen] = useState(false)
  const arrowRef = useRef(null)
  const {refs, floatingStyles, context} = useFloating({
    open: isOpen,
    onOpenChange: setIsOpen,
    middleware: [flip(), shift({padding: 20}), arrow({element: arrowRef}), offset(5)]
  })

  const dismiss = useDismiss(context)
  const click = useClick(context)
  const {getReferenceProps, getFloatingProps} = useInteractions([click, dismiss])

  return {
    isOpen,
    referenceProps: {ref: refs.setReference, ...getReferenceProps()},
    floatingProps: {ref: refs.setFloating, style: floatingStyles, ...getFloatingProps()},
    arrowProps: {ref: arrowRef, context}
  }
}

const Tooltip = ({children, tooltipElement}) => {
  const {isOpen, referenceProps, floatingProps, arrowProps} = useTooltip()
  return (
    <>
      {React.Children.map(children, child => React.cloneElement(child, referenceProps))}
      {isOpen && (
        <FloatingPortal>
          <StyledTooltip {...floatingProps}>
            <FloatingArrow {...arrowProps} />
            {React.cloneElement(tooltipElement)}
          </StyledTooltip>
        </FloatingPortal>
      )}
    </>
  )
}

export default Tooltip
