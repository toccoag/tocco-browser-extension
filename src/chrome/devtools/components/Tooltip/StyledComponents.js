import styled from 'styled-components'

export const StyledTooltip = styled.div`
  pointer-events: none;
  background-color: #eee;
  border: 1px solid #bbb;
  z-index: 10;
  padding: 10px;

  & svg path {
    /* style FloatingArrow SVG */
    fill: #eee;
    stroke: #bbb;
  }
`
