import EntityModelContextProvider from './EntityModelContext'
import RequestListener from './RequestListener'
import {StyledMainTitle} from './StyledComponents'
import TabContextProvider from './TabContext'

const App = () => {
  return (
    <TabContextProvider>
      <EntityModelContextProvider>
        <div>
          <StyledMainTitle>Tocco Devtools</StyledMainTitle>
          <RequestListener />
        </div>
      </EntityModelContextProvider>
    </TabContextProvider>
  )
}

export default App
