import {StyledRequestTooltip} from '../StyledComponents'

const RequestTooltip = ({request}) => {
  return (
    <StyledRequestTooltip>
      <span>URL</span>
      <span>{request.request.url}</span>

      <span>Method</span>
      <span>{request.request.method}</span>

      <span>Body</span>
      <span>{request.request.postData?.text || '{}'}</span>
    </StyledRequestTooltip>
  )
}

export default RequestTooltip
