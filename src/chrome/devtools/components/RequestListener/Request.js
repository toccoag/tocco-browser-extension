import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCircleInfo} from '@fortawesome/free-solid-svg-icons'

import {StyledButtonIcon, StyledCheckbox, StyledRequest} from '../StyledComponents'
import RequestTooltip from './RequestTooltip'
import Tooltip from '../Tooltip'

const Request = ({selected, onSelectChange, request}) => {
  return (
    <label>
      <StyledCheckbox type="checkbox" checked={selected} onChange={e => onSelectChange(e.target.checked)} />
      <StyledRequest>{request.request.url}</StyledRequest>
      <Tooltip tooltipElement={<RequestTooltip request={request} />}>
        <StyledButtonIcon>
          <FontAwesomeIcon icon={faCircleInfo} />
        </StyledButtonIcon>
      </Tooltip>
    </label>
  )
}

export default Request
