import {faPlay, faStop, faTrash} from '@fortawesome/free-solid-svg-icons'
import {useEffect, useRef, useState} from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {v4 as uuidv4} from 'uuid'

import {StyledButton, StyledButtonLight, StyledList, StyledListItem, StyledListenerTitle} from '../StyledComponents'
import {isPreferencesRequest, isValidRequest} from './utils'
import Request from './Request'
import RequestExport from './RequestExport'
import {listenToXhr} from '../../../chrome-api'

const RequestListener = () => {
  const [active, setActive] = useState(false)
  const [requests, setRequests] = useState([])
  const [selectedRequests, setSelectedRequests] = useState([])

  const removeListenerCallback = useRef(null)

  const selectRequest = uuid => {
    setSelectedRequests(currentSelectedRequests => [...currentSelectedRequests, uuid])
  }
  const deselectRequest = uuid => {
    setSelectedRequests(currentSelectedRequests => currentSelectedRequests.filter(u => u !== uuid))
  }

  const addRequest = req => {
    setRequests(currentRequests => [...currentRequests, req])
    if (!isPreferencesRequest(req.request)) {
      // usually the preferences are not that interesting - do not check per default
      selectRequest(req.uuid)
    }
  }

  const reset = () => {
    setRequests([])
    setSelectedRequests([])
  }

  useEffect(() => {
    if (active) {
      const removeListener = listenToXhr(request => {
        if (isValidRequest(request)) {
          addRequest({uuid: uuidv4(), ...request})
        }
      })
      removeListenerCallback.current = removeListener
    } else if (removeListenerCallback.current) {
      removeListenerCallback.current()
    }
  }, [active])

  const requestsToExport = requests.filter(({uuid}) => selectedRequests.includes(uuid))

  return (
    <div>
      <StyledListenerTitle>Cypress Testdaten</StyledListenerTitle>
      <StyledButton onClick={() => setActive(!active)}>
        {active ? <FontAwesomeIcon icon={faStop} /> : <FontAwesomeIcon icon={faPlay} />}
        {active ? ' Stoppen' : ' Aufzeichnen'}
      </StyledButton>
      <StyledButtonLight onClick={() => reset()}>
        <FontAwesomeIcon icon={faTrash} /> Zurücksetzen
      </StyledButtonLight>
      <StyledList>
        {requests.map(req => (
          <StyledListItem key={req.uuid}>
            <Request
              selected={selectedRequests.includes(req.uuid)}
              onSelectChange={selected => (selected ? selectRequest(req.uuid) : deselectRequest(req.uuid))}
              request={req}
            />
          </StyledListItem>
        ))}
      </StyledList>
      <RequestExport requests={requestsToExport} />
    </div>
  )
}

export default RequestListener
