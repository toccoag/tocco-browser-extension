import {camelCase, kebabCase} from 'lodash'
import {useEffect, useState} from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faFileCode} from '@fortawesome/free-solid-svg-icons'

import {StyledButton, StyledInput} from '../StyledComponents'
import {getCodeHeader, getCodeFooter, getCodeBody} from './utils'
import Code from './Code'
import Help from './Help'
import {useEntityModelContext} from '../EntityModelContext'
import {useTabContext} from '../TabContext'

const InitialSeedName = 'Seed name'

const RequestExport = ({requests}) => {
  const [exportedRequests, setExportedRequests] = useState('')
  const [seedName, setSeedName] = useState(InitialSeedName)
  const {entityModel} = useEntityModelContext()
  const {url} = useTabContext()

  const exportRequests = async () => {
    const codeBody = await getCodeBody(url.origin, entityModel, requests)
    setExportedRequests(getCodeHeader() + codeBody + getCodeFooter())
  }

  useEffect(() => {
    if (requests.length === 0) {
      setSeedName(InitialSeedName)
      setExportedRequests('')
    }
  }, [requests])

  return (
    <>
      {requests.length !== 0 && (
        <StyledButton onClick={exportRequests}>
          <FontAwesomeIcon icon={faFileCode} /> Seed generieren
        </StyledButton>
      )}
      {exportedRequests && exportedRequests.length > 0 && (
        <>
          <Help seedName={seedName} />
          <StyledInput value={seedName} onChange={e => setSeedName(e.target.value)} />
          <Code value={exportedRequests} />
          <Code value={`'db:seed:${kebabCase(seedName)}': () => seeds.${camelCase(seedName)}.seed(config)`} />
        </>
      )}
    </>
  )
}

export default RequestExport
