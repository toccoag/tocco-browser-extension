import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCopy} from '@fortawesome/free-solid-svg-icons'
import {useRef} from 'react'

import {StyledCodeCopyButton, StyledCodeWrapper, StyledCode} from '../StyledComponents'

const Code = ({value}) => {
  const element = useRef()

  const copyToClipboard = () => {
    element.current.select()
    document.execCommand('copy')
  }

  return (
    <StyledCodeWrapper>
      <StyledCode value={value} ref={element} />
      <StyledCodeCopyButton onClick={copyToClipboard}>
        <FontAwesomeIcon icon={faCopy} />
      </StyledCodeCopyButton>
    </StyledCodeWrapper>
  )
}

export default Code
