import {camelCase, kebabCase} from 'lodash'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCircleInfo} from '@fortawesome/free-solid-svg-icons'

import {StyledButtonIcon} from '../StyledComponents'
import Tooltip from '../Tooltip'

const Help = ({seedName}) => {
  return (
    <Tooltip
      tooltipElement={
        <div>
          Den gewünschten Namen für den neuen DB-Seed im Eingabefeld eintippen.
          <br />
          Den Inhalt der oberen Textbox in ein neues File im Ordner &apos;cypress/plugins/seeds/{camelCase(seedName)}
          .js&apos; kopieren.
          <br />
          Den Inhalt der unteren Textbox in &apos;cypress/plugins/index.js&apos; eintragen.
          <br />
          Im Cypress-Test mit &apos;cy.task(&apos;db:seed:{kebabCase(seedName)}&apos;)&apos; verwenden.
        </div>
      }
    >
      <StyledButtonIcon>
        <FontAwesomeIcon icon={faCircleInfo} />
      </StyledButtonIcon>
    </Tooltip>
  )
}

export default Help
