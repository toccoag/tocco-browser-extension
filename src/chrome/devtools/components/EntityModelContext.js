import {createContext, useContext, useEffect, useState} from 'react'

import {fetchEntityModel} from '../../../tocco-api'
import {useTabContext} from './TabContext'

const defaultValue = {loading: false, entityModel: null}
const EntityModelContext = createContext(defaultValue)

const EntityModelContextProvider = ({children}) => {
  const [loading, setLoading] = useState(false)
  const [entityModel, setEntityModel] = useState(null)
  const {url} = useTabContext()

  const getEntityModel = async () => {
    setLoading(true)
    try {
      const model = await fetchEntityModel(url.origin)
      setEntityModel(model)
    } catch (e) {
      console.error(e)
      setEntityModel(null)
    }
    setLoading(false)
  }

  useEffect(() => {
    getEntityModel()
  }, [])

  const value = {
    loading,
    entityModel
  }

  return <EntityModelContext.Provider value={value}>{children}</EntityModelContext.Provider>
}

export const useEntityModelContext = () => useContext(EntityModelContext)
export default EntityModelContextProvider
