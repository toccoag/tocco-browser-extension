import {createContext, useContext, useEffect, useState} from 'react'

import {getAttachedTab} from '../../chrome-api'

const defaultValue = null
const TabContext = createContext(defaultValue)

const TabContextProvider = ({children}) => {
  const [loading, setLoading] = useState(true)
  const [tab, setTab] = useState(null)

  const getTab = async () => {
    setLoading(true)
    try {
      const tab = await getAttachedTab()

      const url = tab ? new URL(tab.url) : null
      setTab({tab, url})
    } catch (e) {
      console.error(e)
      setTab(null)
    }
    setLoading(false)
  }

  useEffect(() => {
    getTab()
  }, [])

  const value = tab
  if (loading) {
    return null
  }

  return <TabContext.Provider value={value}>{children}</TabContext.Provider>
}

export const useTabContext = () => useContext(TabContext)
export default TabContextProvider
