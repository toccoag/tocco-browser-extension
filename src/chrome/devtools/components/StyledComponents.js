import styled, {css} from 'styled-components'

export const ToccoColor = '#B22A31'

export const Title = css`
  font-size: 14px;
`

export const SubTitle = css`
  font-size: inherit;
  font-weight: normal;
`

export const Button = css`
  border: 1px solid ${ToccoColor};
  background-color: ${ToccoColor};
  color: white;
  border-radius: 40px;
  line-height: 1;
  outline: none;
  cursor: pointer;
  padding: 5px 7px;
  text-align: center;

  &:disabled {
    color: #999;
    border-color: #999;
    background-color: white;
    cursor: not-allowed;
  }
`

export const ButtonLight = css`
  ${Button}
  border: 1px solid ${ToccoColor};
  background-color: white;
  color: ${ToccoColor};
`

export const ButtonLink = css`
  border: 0px;
  padding: 0px;
  outline: none;
  cursor: pointer;
  text-decoration: underline;
  color: ${ToccoColor};
  background-color: transparent;
  text-align: left;
  display: inline-block;
`

export const StyledMainTitle = styled.h1`
  color: ${ToccoColor};
  font-size: 20px;
  margin: 0px 0px 20px;
`

export const StyledListenerTitle = styled.h2`
  ${Title}
  font-size: 16px;
`

export const StyledPartTitle = styled.h3`
  ${Title}
`

export const StyledButton = styled.button`
  ${Button}
  margin-right: 10px;

  &:last-child {
    margin-right: 0px;
  }
`

export const StyledButtonIcon = styled.button`
  border: none;
  background-color: transparent;
`

export const StyledButtonLight = styled.button`
  ${ButtonLight}
  margin-right: 10px;

  &:last-child {
    margin-right: 0px;
  }
`
export const StyledList = styled.ul`
  margin: 20px 0;
  padding: 0;
  list-style-type: none;
`

export const StyledListItem = styled.li`
  margin: 0;
  padding: 0;
  display: flex;
  align-items: center;
`

export const StyledRequest = styled.span`
  margin-left: 10px;
`

export const StyledCheckbox = styled.input``

export const StyledCode = styled.textarea`
  font-size: 12px;
  line-height: 1.5;
  white-space: pre-line;
  border: 1px solid #999;
  padding: 5px;
  background-color: #fafafa;
  display: block;
  width: calc(100% - 30px);
  margin: 10px;
  field-sizing: content;
`

export const StyledCodeWrapper = styled.div`
  position: relative;
`

export const StyledCodeCopyButton = styled.button`
  position: absolute;
  top: 2px;
  right: 11px;
  ${ButtonLight}
`

export const StyledRequestTooltip = styled.div`
  display: grid;
  grid-template-columns: 50px auto;
  grid-gap: 5px;
`

export const StyledText = styled.textarea`
  white-space: pre-line;
  border: 1px solid #999;
  padding: 5px;
  background-color: #fafafa;
  display: block;
  width: calc(100% - 30px);
  margin: 10px;
  field-sizing: content;
`

export const StyledInput = styled.input`
  display: block;
  margin: 10px;
`
