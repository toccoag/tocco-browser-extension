const DOM_MESSAGE_ID = 'getDOM'

export const getCurrentTab = async () => {
  const queryOptions = {active: true, lastFocusedWindow: true}
  const [tab] = await chrome.tabs.query(queryOptions)
  return tab
}

export const getCurrentUrl = async () => {
  const tab = await getCurrentTab()
  return tab ? new URL(tab.url) : undefined
}

export const navigateTo = redirectUrl => {
  chrome.tabs.update(undefined, {url: redirectUrl})
}

export const openTab = url => {
  chrome.tabs.create({url, active: true})
}

export const getDOM = async callback => {
  const tab = await getCurrentTab()
  if (tab) {
    chrome.tabs.sendMessage(tab.id, {text: DOM_MESSAGE_ID}, callback)
  }
}

/********* Devtools ***********/
export const createDevtoolToccoPanel = () => chrome.devtools.panels.create('⚡️ Tocco', '', 'tocco_devtool_panel.html') // Icon somehow not working

// getCurrentTab returns undefined if devtools are undocked => get attached tab in devtools
export const getAttachedTab = async () => {
  const targets = await chrome.debugger.getTargets()
  const [attached] = targets.filter(target => target.type === 'page' && target.attached)

  return new Promise(resolve => {
    chrome.tabs.get(attached.tabId, resolve)
  })
}

export const listenToXhr = callback => {
  chrome.devtools.network.onRequestFinished.addListener(callback)
  return () => chrome.devtools.network.onRequestFinished.removeListener(callback)
}
