const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  mode: process.env.NODE_ENV,
  entry: {
    popup: './src/chrome/popup/popup.js',
    tocco_devtools: './src/chrome/devtools/devtools.js',
    tocco_devtool_panel: './src/chrome/devtools/panel.js'
  },
  output: {
    path: path.resolve(__dirname, '../..', 'dist/chrome'),
    filename: '[name].[fullhash].js'
  },
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localsConvention: 'camelCase',
              sourceMap: true
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {from: 'src/chrome/manifest.json'},
        {from: 'src/chrome/popup/tocco-circle.png'},
        {from: 'src/chrome/content.js'},
        {from: 'icons/*', context: 'src/chrome'}
      ]
    }),
    new HtmlWebpackPlugin({
      filename: 'popup.html',
      template: 'public/index.html',
      chunks: ['popup']
    }),
    new HtmlWebpackPlugin({
      filename: 'tocco_devtools.html',
      template: 'public/index.html',
      chunks: ['tocco_devtools']
    }),
    new HtmlWebpackPlugin({
      filename: 'tocco_devtool_panel.html',
      template: 'public/index.html',
      chunks: ['tocco_devtool_panel']
    })
  ]
}
