const ATTRIBUTE_WIDGET_KEY = 'data-tocco-widget-key'

export const getWidgetConfigKeys = doc => {
  const widgetContainerNodeList = doc.querySelectorAll(`[${ATTRIBUTE_WIDGET_KEY}]`)
  const widgetContainers = Array.prototype.slice.call(widgetContainerNodeList)
  const widgetConfigKeys = widgetContainers.map(c => c.getAttribute(ATTRIBUTE_WIDGET_KEY))
  return widgetConfigKeys
}

export const getWidgetDomain = doc => {
  const scripts = Array.prototype.slice.call(doc.scripts)
  const bootstrapScript = scripts.find(s => s.src.includes('js/tocco-widget-utils/dist/bootstrap.js'))
  if (bootstrapScript) {
    const {protocol, host} = new URL(bootstrapScript.src)
    const baseUrl = protocol + '//' + host
    return baseUrl
  }
  return null
}
