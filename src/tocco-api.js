const getHeaders = () => {
  const headers = new Headers()
  headers.set('Content-Type', 'application/json')
  return headers
}

export const fetchClientSettings = async baseUrl => {
  const response = await fetch(`${baseUrl}/nice2/rest/client/settings`)
  if (response.ok) {
    const body = await response.json()
    return body
  }

  throw new Error('Could not fetch client settings.')
}

export const fetchConfigurations = async baseUrl => {
  const response = await fetch(`${baseUrl}/nice2/rest/reloadConfiguration`)
  const body = await response.json()
  return body.configurations
}

export const reloadConfigurations = async (baseUrl, ids) => {
  const options = {
    method: 'POST',
    body: JSON.stringify(ids),
    headers: getHeaders()
  }
  await fetch(`${baseUrl}/nice2/rest/reloadConfiguration`, options)
}

export const fetchProperties = async (baseUrl, properties) => {
  const options = {
    method: 'POST',
    body: JSON.stringify({propertyNames: properties}),
    headers: getHeaders()
  }
  const response = await fetch(`${baseUrl}/nice2/rest/client/property`, options)
  const body = await response.json()
  return body.loadedProperties
}

export const fetchWidgetDomains = async baseUrl => {
  const data = {
    limit: 100,
    sort: 'domain asc',
    offset: 0,
    paths: ['domain']
  }

  const options = {
    method: 'POST',
    body: JSON.stringify(data),
    headers: getHeaders()
  }

  const response = await fetch(`${baseUrl}/nice2/rest/entities/2.0/Widget_domain/search`, options)
  const body = await response.json()
  return body.data
}

export const fetchWidgetConfig = async (baseUrl, widgetConfigKey) => {
  const response = await fetch(`${baseUrl}/nice2/rest/widget/configs/${widgetConfigKey}`)
  const body = await response.json()
  return body
}

export const fetchEntityModel = async baseUrl => {
  const response = await fetch(`${baseUrl}/nice2/rest/entities?_fullModel=true&_omitLinks=true`)
  const body = await response.json()
  return body
}

export const fetchUniqueId = async (baseUrl, entityModel, pk) => {
  const response = await fetch(
    `${baseUrl}/nice2/rest/entities/2.0/${entityModel}/${pk}?_paths=unique_id&_omitLinks=true`
  )
  const body = await response.json()
  return body.paths.unique_id.value
}
