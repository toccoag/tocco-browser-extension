export const isNiceVersionGreaterOrEqThan = (niceVersion, compareVersion) => {
  const v1 = niceVersion.split('.', 2).map(d => parseInt(d, 10))
  const v2 = compareVersion.split('.', 2).map(d => parseInt(d, 10))

  const compare1 = v1[0] - v2[0]
  const result = compare1 === 0 ? v1[1] - v2[1] : compare1
  return result >= 0
}
