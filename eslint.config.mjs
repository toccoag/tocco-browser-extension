import globals from 'globals'
import js from '@eslint/js'
import pluginReact from 'eslint-plugin-react'

export default [
  {files: ['**/*.{js,mjs,cjs,jsx}']},
  {languageOptions: {globals: {...globals.browser, chrome: 'readonly'}}},
  js.configs.recommended,
  pluginReact.configs.flat.recommended,
  pluginReact.configs.flat['jsx-runtime'],
  {
    settings: {
      react: {
        version: 'detect'
      }
    }
  },
  {
    rules: {
      curly: [2, 'all'],
      'brace-style': [2, '1tbs'],
      'key-spacing': [2, {beforeColon: false, afterColon: true, mode: 'strict'}],
      semi: [2, 'never'],
      'react/react-in-jsx-scope': 'off',
      'react/prop-types': 'off',
      'sort-imports': [
        'error',
        {
          ignoreCase: false,
          ignoreDeclarationSort: false,
          ignoreMemberSort: true,
          memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single'],
          allowSeparatedGroups: true
        }
      ],
      'no-unused-vars': [
        'error',
        {
          argsIgnorePattern: '^_',
          varsIgnorePattern: '^_',
          caughtErrorsIgnorePattern: '^_'
        }
      ]
    }
  },
  {
    ignores: ['dist/*', '**/webpack.config.js']
  }
]
