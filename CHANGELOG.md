# Changelog

## 1.5.0

- improve Cypress test data recorder

  - able to have dev tools undocked
  - filter out property REST calls
  - able to copy generated code to clipboad

- improve dev experience by adding linter and prettier
- add release pipelines

## 1.4.0

- add Dev tools for Cypress test data recording

## 1.3.1

- fix nice version handling for widget showcase link

## 1.3.0

- Widget detection and links

## 1.2.0

- show useful links:

  - Status Tocco page
  - Widget-Showcase (>=3.7)
  - Widget-Domains

- deps update

## 1.1.0

- open (new or legacy) Admin in new tab

## 1.0.0

- initial version
