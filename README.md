# Tocco Browser Extension

## Getting started

### Chrome extension

Build extension and load to chrome

```
yarn build:chrome
```

- Open `chrome://extensions/` and activate developer view
- "load unpacked" extension => dist/chrome

When the extension is loaded into chrome it will refresh automatically on file changes.

Therefore it's easiest to build the plugin as soon as you've changed some files.

```
yarn build:chrome:watch
```

### Code quality

#### ESLint

```
yarn lint
```

#### Prettier

```
yarn format
```

### Releasing

1. Fill in [CHANGELOG.md](/CHANGELOG.md)

2. Bump version in [package.json](/package.json)

3. Commit changes and merge to `master`

4. Tag commit on `master` with same version as in `package.json`

```
git tag 1.3.0
git push -u origin 1.3.0
```

The extension is build and published to the [package registry](https://gitlab.com/toccoag/tocco-browser-extension/-/packages). After that a [Gitlab release](https://gitlab.com/toccoag/tocco-browser-extension/-/releases) is created with the package files to download.

5. Announce new release in the `#tocco` Slack-Channel
